from __future__ import print_function

import logging
import os
import pkg_resources
import random
import shutil
import sys
import zc.recipe.egg


class Recipe(object):
    def __init__(self, buildout, name, options):
        self.log = logging.getLogger(name)
        self.egg = zc.recipe.egg.Egg(buildout, options['recipe'], options)
        self.buildout, self.name, self.options = buildout, name, options

        options['location'] = os.path.join(buildout['buildout']['parts-directory'], name)
        options['bin-directory'] = buildout['buildout']['bin-directory']
        options.setdefault('project', 'project')
        options.setdefault('settings', 'development')
        options.setdefault('initialization', '')
        options.setdefault('logfile', '')

        if 'extra-path' in options:
            options['pythonpath'] = options['extra-paths']
        else:
            options.setdefault('extra-paths', options.get('pythonpath', ''))

    def install(self):
        base_dir = self.buildout['buildout']['directory']
        project_dir = os.path.join(base_dir, 'src', self.options['project'])
        extra_paths = self.get_extra_paths()
        requirements, ws = self.egg.working_set(['sergelab.flask.zcrecipe'])
        script_paths = []

        # Create Flask management script
        script_paths.extend(self.__create_manage_script(extra_paths, ws))

        if not self.options.get('projectegg'):
            self.__create_project(project_dir)

        return script_paths

    def __create_manage_script(self, extra_paths, ws):
        project = self.options.get('projectegg', self.options['project'])

        return zc.buildout.easy_install.scripts(
            [(self.options.get('control-script', self.name), '{0}.manage'.format(project), 'main')],
            ws,
            sys.executable,
            self.options['bin-directory'],
            extra_paths=extra_paths,
            initialization=self.options['initialization']
        )

    def __create_project(self, project_dir):
        try:
            self.preset = pkg_resources.resource_filename('sergelab.flask.zcrecipe', 'preset')

            if self.preset == None:
                raise Exception("Preset doesn't exist.")

            if not os.path.exists(project_dir):
                shutil.copytree(self.preset, project_dir, ignore=shutil.ignore_patterns())
        finally:
            pkg_resources.cleanup_resources(True)

        config_dir = os.path.join(project_dir, 'config')

        folders = [os.path.join(project_dir, 'config'),
                   os.path.join(project_dir, 'media'),
                   os.path.join(project_dir, 'static'),
                   os.path.join(project_dir, 'translations')]

        for folder in folders:
            self.__create_folder(folder)

        files = [(os.path.join(project_dir, '__init__.py'), '', {}),
                 (os.path.join(config_dir, '__init__.py'), '', {})]

        for filename, template, options in files:
            self.__create_file(filename, template, options)

        template_vars = {'secret': self.__generate_secret()}
        template_vars.update(self.options)

        # Parse some project files
        files_to_parse = [os.path.join(project_dir, 'manage.py'),
                          os.path.join(config_dir, 'base.cfg'),
                          os.path.join(config_dir, 'production.cfg')]

        for filename in files_to_parse:
            self.__parse_file(filename, template_vars)

    def get_extra_paths(self):
        extra_paths = [self.buildout['buildout']['directory']]
        python_path = [p.replace('/', os.path.sep) for p in self.options['extra-paths'].splitlines() if p.strip()]
        extra_paths.extend(python_path)

        return extra_paths

    def __create_folder(self, foldername):
        if not os.path.exists(foldername):
            print('Create folder {0} ... '.format(foldername), end='')

            try:
                os.makedirs(foldername)
                print('ok')
            except Exception as e:
                print('fail')
                self.log.exception(e)
                raise e

    def __create_file(self, filename, template='', options={}):
        if os.path.exists(filename):
            return

        print('Create file {0} ... '.format(filename), end='')

        f = open(filename, 'w')

        try:
            f.write(template.format(**options))
            print('ok')
        except Exception as e:
            print('fail')
            self.log.exception(e)
            raise e
        finally:
            f.close()

    def __generate_secret(self):
        chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
        return ''.join([random.choice(chars) for i in range(50)])

    def __parse_file(self, filename, options={}):
        if not os.path.exists(filename):
            return

        f = open(filename, 'r')
        data = None

        try:
            data = f.read()
        except Exception as e:
            raise e

        f.close()
        f = open(filename, 'w')

        try:
            for opt in options:
                data = data.replace('{%s}' % opt, options[opt])
            f.write(data)
        except Exception as e:
            raise e
