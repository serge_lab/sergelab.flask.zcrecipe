# coding: utf-8
from flask import render_template

from .init import app


@app.route('/')
def index():
    return render_template('index.html')


# ---=== Error pages ===---


@app.errorhandler(401)
def http_401_unauthorized(error):
    return render_template('401.html', error=error), 401


@app.errorhandler(404)
def http_404_not_found(error):
    return render_template('404.html', error=error), 404


@app.errorhandler(500)
def http_500_internal_server_error(error):
    return render_template('500.html', error=error), 500


@app.errorhandler(501)
def http_501_not_implemented(error):
    return render_template('501.html', error=error), 501
