# coding: utf-8
from flask import current_app
from flask.ext.assets import ManageAssets
from flask.ext.script import Server, Manager
from flask.ext.script.commands import Clean, ShowUrls

from .init import app, collect


manager = Manager(app)

manager.add_command('clean', Clean())
manager.add_command('routes', ShowUrls())
manager.add_command('assets', ManageAssets())
manager.add_command('runserver', Server())

collect.init_script(manager)


def main():
    manager.run()
