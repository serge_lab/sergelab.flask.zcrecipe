# coding: utf-8
from flask import Blueprint, Flask
from flask.ext.assets import Environment
from flask.ext.collect import Collect
from flask.ext.debugtoolbar import DebugToolbarExtension


class MyCollect(Collect, object):
    def init_app(self, app):
        super(MyCollect, self).init_app(app)
        self.blueprints['__app'] = app


# Extensions
assets = Environment()
collect = MyCollect()


def create_app(name):
    app = Flask(name)
    app.config.from_pyfile('config/base.cfg')
    app.config.from_envvar('FLASK_SETTINGS')

    assets.init_app(app)
    collect.init_app(app)

    return app


def init_extensions(app):
    # Enable the DebugToolbar
    if app.config.get('DEBUG_TOOLBAR'):
        DebugToolbarExtension(app)

    # Enable Jinja2 extensions
    if not app.config.get('JINJA2_EXTENSIONS'):
        app.config['JINJA2_EXTENSIONS'] = []

    for j2e in app.config['JINJA2_EXTENSIONS']:
        app.jinja_env.add_extension(j2e)


def register_blueprints(app):
    rv = []

    if not app.config.get('BLUEPRINTS'):
        app.config['BLUEPRINTS'] = []

    for name in app.config['BLUEPRINTS']:
        m = importlib.import_module(name + '.views')

        for item in dir(m):
            item = getattr(m, item)

            if isinstance(item, Blueprint):
                app.register_blueprint(item)
                app.logger.debug('Blueprint {0} registered'.format(name))
                rv.append(item)

    return rv

app = create_app(__name__)

with app.app_context():
    register_blueprints(app)
    init_extensions(app)

    import models
    import views
