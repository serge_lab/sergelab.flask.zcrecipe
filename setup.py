import os
import fnmatch

from setuptools import find_packages, setup


def find_package_data(package, src='', filter='*'):
    matches = []
    where = package.replace('.', os.sep)

    for root, dirnames, filenames in os.walk(os.path.join(where, src)):
        for filename in fnmatch.filter(filenames, filter):
            matches.append(os.path.relpath(os.path.join(root, filename), where))

    return {package: matches}


setup_args = dict(name='sergelab.flask.zcrecipe',
                  author='Sergey Syrov',
                  author_email='sergelab@gmail.com',
                  url='https://bitbucket.org/serge_brpr/sergelab.flask.zcrecipe/',
                  version='0.1',
                  packages=find_packages('.'),
                  description='Buildout recipe for Flask projects',
                  long_description=open('README.rst').read(),
                  namespace_packages=['sergelab',
                                      'sergelab.flask'],
                  install_requires=['zc.buildout'],
                  package_dir={'': '.'},
                  package_data=find_package_data('sergelab.flask.zcrecipe'),
                  entry_points={'zc.buildout': ['default = sergelab.flask.zcrecipe.recipe:Recipe']},
                  zip_safe=True)


if __name__ == '__main__':
    setup(**setup_args)
